module.exports = {
    publicPath: '/', //vue的默认路径，我的@vue/cli 是4.5.13版本，baseURL已经被弃用了，要使用publicPath
    outputDir: 'dist', //打包时生成的文件名，里面全是构架好了的
    //assetsDit: "assets",//静态资源存放的文件夹
    lintOnSave: false, //是否开启eslint保存检测，有效值有：true，false，'error'，当选择error时，如果检查出错误会触发编译失败
    devServer: {
             open: false,//项目启动时是否自动打开浏览器
            // host: "localhost",//前端对应主机名
             port:  8849,//前端项目启动的端口号
             https: false,//是否开启协议名，如果开启会发出警告
             hotOnly: false,//是否开启热模块更新（webpack已经给我们实现了这个，这里选false就可以）
             proxy:{   //代理设置，可以设置多个跨域代理
                 "/api":{    //配置跨域的名字
                     target:"http://localhost:8848/wenliGreenShop",//跨域的地址
                     ws:true,
                     changeOrigin:true,  //是否允许跨域
                     pathRewrite:{
                         "^/api":"" ////这里理解成用‘/api’代替target里面的地址，后面组件中我们调用接口时直接用api代替 比如我要调用'http://40.00.100.100:3002/user/add'，直接写‘/api/user/add’即可
                     }
                 }
             }
         /*  proxy: "http://localhost:8848/wenliGreenShop",
           port: 8849*/
    }

};