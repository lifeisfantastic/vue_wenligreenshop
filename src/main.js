import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'  //按需导入
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import VueCookies from 'vue-cookies'
import Viewer from 'v-viewer'          //引入Viewer插件就可以使用图片预览效果了
import 'viewerjs/dist/viewer.css'

Vue.config.productionTip = false
Vue.use(VueCookies)
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'; //后端默认接受这种类型的数据，要用@RequestBody接受application/json这种vue默认的请求
Vue.prototype.$axios=axios
Vue.use(Viewer);
Vue.prototype.bus = new Vue()
new Vue({
  router,//路由
  store,//根实例状态管理
  render: h => h(App)//es6写法，等价于reder:function(createElement){return createElement(App)}，这里的h就是createElement()函数，vue会使用render来渲染实例的DOM树
}).$mount('#app')//将其渲染到<div id="app"></div>里，与原来的el属性没有区别，只是换了一种写法
