import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isPractice: false, //练习模式标志
    flag: false, //菜单栏左右滑动标志
    userInfo: null,
    menu: [{    //这些全是后台的数据
      title: '数据监控',
      path: '/adminIndex',
      icon: 'el-icon-s-marketing',
    },
      {
        title: '产品管理',
        path: '/product',
        icon: 'el-icon-s-goods',
      },
      {
        title: '产品分类',
        path: '/category',
        icon: 'el-icon-s-order',
      },
      {
        title: '用户管理',
        path: '/user',
        icon: 'el-icon-user',
      },
      {
        title: '订单管理',
        path: '/order',
        icon: 'el-icon-document',
      },
      {
        title: '账户设置',
        path: '/adminSetting',
        icon: 'el-icon-setting',
      },
    ],
  },
  mutations:{
    practice(state,status) {
      state.isPractice = status
    },
    toggle(state) {//开关
      state.flag = !state.flag
    },
    changeUserInfo(state,info) {
      state.userInfo = info
    }
  },
  actions: {
    getUserInfo(context, info) {
      context.commit('changeUserInfo', info)
    },
    getPractice(context, status) {
      context.commit('practice', status)
    }
    /*,
    modules: {}*/
  }
})
