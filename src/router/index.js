import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)
const router = new VueRouter({
  mode: 'history',
  //base: process.env.BASE_URL,

  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('../components/common/Login') //@/就是src的路径别名
    },

    {
      path: '/adminIndex',  //管理主页
      name: 'AdminIndex',   //这里写top，bottom，与left
      component: () => import('../components/admin/Index'),//这里写vue组件路径，组件里记得写<mainLeft></mainLeft>，这样就可以一直显示菜单栏了
      children: [  //然后所有的组件都将在UserIndex里显示，要记得在UserIndex组件里写<router-view></router-view>，这样他的子路由就会在里面嵌套显示
        {
          path: '/product',
          name: 'Product',
          component: () => import('../components/admin/product'),
        },
        {
          path: '/user',
          name: 'User',
          component: ()=> import('../components/admin/user')
        },
        {
          path: '/category',
          name: 'Category',
          component: ()=> import('@/components/admin/category')
        },
        {
          path: '/adminSetting',
          name: 'AdminSetting',
          component: ()=> import('@/components/admin/adminSetting')
        },
        {
          path: '/productDetail',
          name: 'ProductDetail',
          component: ()=> import('@/components/admin/productDetail')
        },
        {
          path: '/order',
          name: 'Order',
          component: ()=> import('@/components/admin/order')
        },
        {
          path: 'orderDetail',
          name: 'OrderDetail',
          component: ()=> import('@/components/admin/orderDetail')
        },
        {
          path: 'userDetail',
          name: 'UserDetail',
          component: () => import('@/components/admin/userDetail')
        },
        {
          path: '/addProduct',
          name: 'AddProduct',
          component: () => import('@/components/admin/addProduct')
        },

      ]

    },

    {
      path:'/goodDetail',
      name:'GoodDetail',
      component:() => import('@/components/user/goodDetail'),
    },
    {
      path:'/shoppingCart',
      name:'ShoppingCart',
      component:() => import('@/components/user/shoppingCart'),
    },
    {
      path:'/myOrder',
      name:'MyOrder',
      component:() => import('@/components/user/myOrder'),
    },
    {
      path:'/userSetting',
      name:'UserSetting',
      component:() => import('@/components/user/userSetting'),
    },
    {
      path:'/Register',
      name:'Register',
      component:() => import('@/components/common/register'),
    },
    {
      path:'/createOrder',
      name:'CreateOrder',
      component:() => import('@/components/user/createOrder'),
    },
    // {
    //   path: '/payMoney',
    //   name: 'PayMoney',
    //   component: () => import('../components/user/payMoney'),
    // },

    {
      path: '/',//记得写<mainLeft></mainLeft>，导航不能忘
      name: 'Index',
      component: () => import('../components/user/userIndex'),
      children: [ //接下来写它的子模块功能

        {
          path: '/goods',
          name: 'Goods',
          component: () => import('@/components/user/goods')
        },

      ]
    },
  ]
})


export default router
